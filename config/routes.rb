
Rails.application.routes.draw do
  get 'welcome/index'
  get 'home/index'
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  devise_scope :user do
   # root to: 'users/sessions#home'
    get 'sign_in', to: 'users/sessions#new'
    get '/users/sign_out', to: 'users/sessions#destroy'
  end

  resources :users do
    resources :posts
  end

  resources :posts do
    member do
      get 'show_when_session_destroy'
    end
  end
  root to: 'posts#home'
  get 'home', to: 'posts#home'
end

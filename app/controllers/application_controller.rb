class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  after_action :add_role
  protect_from_forgery with: :null_session
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :phone_number, :avatar, :avatar_file_name, :avatar_content_type, :avatar_file_size])

     devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:first_name, :last_name, :email, :phone_number, :password, :current_password, :password_confirmation, :avatar, :avatar_file_name, :avatar_content_type, :avatar_file_size) }
  end
  def create
    @user = User.create(user_params)
    #user.has_role? :admin
  end

  private

  def add_role
    @user = User.last
    @user.add_role :blogger
    @user.save
  end

# Use strong_parameters for attribute whitelisting
# Be sure to update your create() and update() controller methods.

  def user_params
    params.require(:user).permit(:avatar)
  end
end

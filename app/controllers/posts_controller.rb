class PostsController < ApplicationController

  def new
    @post = current_user.posts.new
    @post.pictures.new
  end

  def create
    @post = current_user.posts.new(post_params)
    return redirect_to root_path if @post.save
    render 'new'
  end

  def index
    @all_posts = Post.all
    @posts = current_user.posts
  end

  def show_when_session_destroy
    @post = Post.find(params[:id])
  end

  def show
    @post = Post.find(params[:id])
  end

  def home
    @posts = Post.all
  end

  def edit
    @post = current_user.posts.find(params[:id])
  end

  def update
    @post = current_user.posts.find(params[:id])
    return redirect_to post_path if @post.update(post_params)
    render 'edit'
  end

  def destroy
    @post = current_user.posts.find(params[:id])
    @post.destroy
    redirect_to root_path
  end

  private

  def post_params
    params.require(:post).permit(:title, :content, :user_id, pictures_attributes: [:id, :post_id, :_destroy, :avatar])
  end
end

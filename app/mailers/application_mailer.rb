class ApplicationMailer < ActionMailer::Base
  default from: 'bespeersmoni@gmail.com'
  layout 'mailer'
end

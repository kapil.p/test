class Picture < ApplicationRecord
  belongs_to :post
  validates :avatar, attachment_presence: true
  has_attached_file :avatar, styles: { medium: '200x200>', thumb: '50x50>', large: '500x500>' }, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/
end

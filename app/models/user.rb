class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_many :posts
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates_length_of :password, minimum: 8
  validates :phone_number, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 1000000000, less_than_or_equal_to: 999999999999}

  validates :avatar, attachment_presence: true
  has_attached_file :avatar, styles: { medium: '200x200>', thumb: '50x50>', large: '500x500>' }, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  after_create :mailer

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def mailer
    UserMailer.welcome_email(self).deliver_now
  end
end

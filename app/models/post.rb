class Post < ApplicationRecord
  belongs_to :user
  validates :title, presence: true
  validates :content, presence: true
  has_many :pictures, inverse_of: :post, dependent: :destroy
  accepts_nested_attributes_for :pictures, allow_destroy: true
end

class AddAvtarUpdatedAtToPictures < ActiveRecord::Migration[5.2]
  def change
    add_column :pictures, :avatar_file_name, :string
    add_column :pictures, :avatar_content_type, :string
    add_column :pictures, :avatar_file_size, :integer
  end
end
